import java.util.*;

public class Main{
    public static void main(String args[]){
	ArrayList<Movie> movies = new ArrayList<Movie>();
	movies.add(new Movie("Home",2021,4));
	movies.add(new Movie("Nizhal",2012,5));
	movies.add(new Movie("Malik", 2020, 3));

	Collections.sort(movies, Movie.movieNameComparator);
	System.out.println("Sort by Name: " + movies);


	Collections.sort(movies, Movie.yearOfReleaseComparator);
	System.out.println("Sort by Year: " + movies);

	Collections.sort(movies, Movie.ratingsComparator);
	System.out.println("Sort by Ratings: " + movies);
    }
}