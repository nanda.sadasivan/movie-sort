import  java.util.*;

    public class Movie{
        
        private String movieName;
        private int yearOfRelease;
        private int ratings;
        
        Movie(String movieName, int yearOfRelease, int ratings){
          this.movieName = movieName;
          this.yearOfRelease = yearOfRelease;
          this.ratings = ratings;
        }
        public void setMovieName(String movieName){
            this.movieName = movieName;
        }
        
        public String getMovieName(){
            return movieName;
        }
        
        public void setYearOfRelease(int yearOfRelease){
            this.yearOfRelease = yearOfRelease;
        }
        
        public int getYearOfRelease(){
            return yearOfRelease;
        }
        
        public void setRatings(int ratings){
            this.ratings = ratings;
        }
        
        public int getRatings(){
            return ratings;
        }
        
        public static Comparator<Movie> movieNameComparator = new Comparator<Movie>(){
            public int compare(Movie m1, Movie m2){
                String movieName1 = m1.getMovieName().toUpperCase();
                String movieName2 = m2.getMovieName().toUpperCase();                
                return movieName1.compareTo(movieName2);
            }
        };
        
        public static Comparator<Movie> yearOfReleaseComparator = new Comparator<Movie>(){
            public int compare(Movie m1, Movie m2){
                int yearOfRelease1 = m1.getYearOfRelease();
                int yearOfRelease2 = m2.getYearOfRelease();
                
                return yearOfRelease1-yearOfRelease2;
            }
        };
       
       public static Comparator<Movie> ratingsComparator = new Comparator<Movie>(){
            public int compare(Movie m1, Movie m2){
                int ratings1 = m1.getRatings();
                int ratings2 = m2.getRatings();
                
                return ratings1-ratings2;
            }
        };

      public String toString(){
	return "{" + movieName + ":" + yearOfRelease + ":" + ratings + "}";
      }        
    }